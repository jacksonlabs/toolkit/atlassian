resource "aws_instance" "terraform-jira" {
  ami             = "${data.aws_ami.centos.id}"
  instance_type   = "t2.medium"
  key_name	  = "${aws_key_pair.aws-jira-key.key_name}"

  security_groups = [
    "${aws_security_group.allow_ssh.name}",
    "${aws_security_group.allow_outbound.name}"
  ]

  provisioner "file" {
    source      = "response-jira.varfile"
    destination = "/tmp/response.varfile"

    connection {
      type          = "ssh"
      user          = "centos"
      private_key   = "${file("aws-jira")}"
    }

  }

  provisioner "file" {
    source      = "provision-jira.sh"
    destination = "/tmp/provision.sh"

    connection {
      type          = "ssh"
      user          = "centos"
      private_key   = "${file("aws-jira")}"
    }

  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/provision.sh",
      "sudo /tmp/provision.sh",
    ]

    connection {
      type          = "ssh"
      user          = "centos"
      private_key   = "${file("aws-jira")}"
    }
  }

  tags {
    Name = "jira"
  }
}


resource "aws_instance" "terraform-bamboo" {
  ami             = "${data.aws_ami.centos.id}"
  instance_type   = "t2.medium"
  key_name	  = "${aws_key_pair.aws-bamboo-key.key_name}"

  security_groups = [
    "${aws_security_group.allow_ssh.name}",
    "${aws_security_group.allow_outbound.name}"
  ]

  provisioner "file" {
    source      = "response-bamboo.varfile"
    destination = "/tmp/response.varfile"

    connection {
      type          = "ssh"
      user          = "centos"
      private_key   = "${file("aws-bamboo")}"
    }

  }

  provisioner "file" {
    source      = "provision-bamboo.sh"
    destination = "/tmp/provision.sh"

    connection {
      type          = "ssh"
      user          = "centos"
      private_key   = "${file("aws-bamboo")}"
    }

  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/provision.sh",
      "sudo /tmp/provision.sh",
    ]

    connection {
      type          = "ssh"
      user          = "centos"
      private_key   = "${file("aws-bamboo")}"
    }
  }

  tags {
    Name = "bamboo"
  }
}

