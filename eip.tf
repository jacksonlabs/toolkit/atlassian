resource "aws_eip" "terraform-jira-eip" {
  instance    = "${aws_instance.terraform-jira.id}"
}

resource "aws_eip" "terraform-bamboo-eip" {
  instance    = "${aws_instance.terraform-bamboo.id}"
}

