#!/bin/bash

yum install -y wget git
yum install -y java-1.8.0-openjdk
wget https://www.atlassian.com/software/bamboo/downloads/binary/atlassian-bamboo-6.5.0.tar.gz

sudo useradd --create-home -c "Bamboo role account" bamboo
sudo mkdir -p /opt/atlassian/bamboo

sudo tar -xvf atlassian-bamboo-6.5.0.tar.gz -C /opt/atlassian/bamboo

sudo chown bamboo: /opt/atlassian/bamboo
sudo ls -ld /opt/atlassian/bamboo

sudo ln -s atlassian-bamboo-6.5.0 /opt/atlassian/bamboo/current

sudo mkdir -p /var/atlassian/application/bamboo
sudo chown bamboo: /var/atlassian/application/bamboo/

sudo mkdir -p /var/atlassian/application/bamboo
sudo chown bamboo: /var/atlassian/application/bamboo
sudo echo "bamboo.home=/var/atlassian/application/bamboo" >> /opt/atlassian/bamboo/current/atlassian-bamboo/WEB-INF/classes/bamboo-init.properties

sudo /opt/atlassian/bamboo/current/bin/start-bamboo.sh



