#!/bin/bash

yum install -y wget git
wget https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.10.0-x64.bin

chmod +x atlassian-jira-software-7.10.0-x64.bin
sudo ./atlassian-jira-software-7.10.0-x64.bin -q -varfile /tmp/response.varfile

sudo /etc/init.d/jira start

# too lazy to get this automated at the moment
#yum install -y postgresql-server postgresql-contrib
#sudo postgresql-setup initdb
#sudo sed -i -e 's/ ident/ md5/g' /var/lib/pgsql/data/pg_hba.conf

#sudo systemctl start postgresql
#sudo systemctl enable postgresql



