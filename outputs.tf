output "jira-server-ip" {
  value = "${aws_eip.terraform-jira-eip.public_ip}"
}

output "bamboo-server-ip" {
  value = "${aws_eip.terraform-bamboo-eip.public_ip}"
}

