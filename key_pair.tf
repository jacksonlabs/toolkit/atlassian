
resource "aws_key_pair" "aws-jira-key" {
  key_name   = "terraform-jira"
  public_key = "${file("aws-jira.pub")}"
}

resource "aws_key_pair" "aws-bamboo-key" {
  key_name   = "terraform-bamboo"
  public_key = "${file("aws-bamboo.pub")}"
}

