# Atlassian

This is a base template for deploying Atlassian tools on AWS using Terraform.

1. Setup Terraform
```
terraform init
```

2. Setup AWS keys
...

## Jira

Setup SSH key
```
ssh-keygen -f aws-jira -t rsa -N ''
```

## Bamboo

Setup SSH key
```
ssh-keygen -f aws-bamboo -t rsa -N ''
```

## Confluence

## Trello

## BitBucket
